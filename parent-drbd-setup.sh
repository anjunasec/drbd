#!/bin/bash

# To query the status 'watch -n 1 /sbin/drbdsetup status r0 --verbose --statistics'

# exit on any error
set -e

DRBD_RES="r0"
HDD_SZ_MB=2048
VSOCK_CID=20
DRBD_DIR="$(dirname "$(readlink -f "$0")")" # Wherever this script is
DRBDADM_BIN="${DRBD_DIR}/drbd-utils/user/v9/drbdadm"
DRBDSETUP_BIN="${DRBD_DIR}/drbd-utils/user/v9/drbdsetup"

print_usage() 
{
    program_name="$0"
    echo "usage: $program_name OPTION [DISKPATH]"
    echo "Options:"
    echo " --build         Build the DRBD modules"
    echo " --disk-create   Create a DRBD disk"
    echo " --start         Start DRBD"
    echo " --stop          Stop DRBD"
    echo " --help          Display this help message"
    exit 0
}

drbd_stop()
{
    sudo losetup -d /dev/loop0 2>/dev/null || true
    sudo "${DRBDADM_BIN}" down "${DRBD_RES}" 2>/dev/null || true
    if grep -q '^drbd_transport_tcp ' /proc/modules; then
        sudo rmmod drbd_transport_tcp
    fi
    if grep -q '^drbd ' /proc/modules; then
        sudo rmmod drbd
    fi
}

drbd_stop_and_exit()
{
    drbd_stop
    exit 0
}

drbd_build() {
    drbd_stop
    sudo chmod 777 .
    sudo yum install -y kernel-devel autoconf automake ocaml ocaml-ocamldoc git patch ocaml-findlib flex
    sudo yum install -y "kernel-devel-uname-r == $(uname -r)"

    pushd "${DRBD_DIR}"

    rm -rf coccinelle drbd drbd-utils

    # The coccinelle provided by the AMI is not latest enough for the drbd, so we build from source
    git clone https://github.com/coccinelle/coccinelle.git
    pushd coccinelle
    git checkout 1.1.1
    ./autogen
    ./configure --disable-doc
    make
    sudo make install
    popd

    git clone https://bitbucket.org/anjunasec/drbd.git
    pushd drbd
    git checkout drbd-9.1
    popd
    make -C drbd clean
    PYTHONPATH=$PYTHONPATH:/usr/local/lib/coccinelle/python make -C drbd

    git clone https://bitbucket.org/anjunasec/drbd-utils.git
    pushd drbd-utils
    ./autogen.sh
    ./configure --prefix=/usr --localstatedir=/var --sysconfdir=/etc --without-manual
    make
    sudo make install
    popd

    sudo chmod 755 .
    popd
    exit 0
}

drbd_init()
{
    local HDD_IMG=$1
    if [[ ! "$HDD_IMG" ]]; then
        USAGE="1";
        print_usage
    fi
    if [[ -f "$HDD_IMG" ]]; then 
        echo "error: file ${HDD_IMG} already exists"
        exit 1
    fi
    
    drbd_stop
    hostname="$(hostname)"
    
    # Create drbd configuration file.
    echo "Creating drbd configuration ..."
    cat << EOM | sudo tee /etc/drbd.d/global_common.conf
global {
    usage-count  no;
}

resource ${DRBD_RES} {

    volume 0 {
                device minor 0;
                disk "/dev/loop0p1";
                meta-disk internal;
        }

    net {
        protocol        A;
        sndbuf-size     2M;
        max-buffers     8000;
        max-epoch-size  8000;
    }

    disk {
        al-extents       6007;
        disk-barrier     no;
        disk-flushes     no;
        al-updates       no;
    }

    on enclave.vm {
        node-id 0;
        address vsock 1:7789;

    volume 0 {
                device minor 0;
                disk "/dev/loop0p1";
                meta-disk internal;
        }
    }

    on ${hostname} {
        node-id 1;
        address vsock 2:7789;
    }
}
EOM
    sudo modprobe lru_cache
    sudo insmod "${DRBD_DIR}/drbd/drbd/drbd.ko"
    sudo insmod "${DRBD_DIR}/drbd/drbd/drbd_transport_tcp.ko" use_vsock=1 "vsock_cid=$VSOCK_CID"

    dd if=/dev/zero "of=${HDD_IMG}" bs=1M "count=${HDD_SZ_MB}"
    sudo parted "${HDD_IMG}" mklabel gpt mkpart primary ext4 1 "${HDD_SZ_MB}"
    sudo losetup -P /dev/loop0 "${HDD_IMG}"
    sudo "${DRBDADM_BIN}" create-md "${DRBD_RES}"
    sudo "${DRBDADM_BIN}" up "${DRBD_RES}"
    sudo "${DRBDADM_BIN}" primary --force "${DRBD_RES}"
    sudo mkfs.ext4 /dev/drbd0

    # a default lost+found directory is created with the file system,
    # we need to remove lost+found directory created after formating the disk
    tmp_dir="$(mktemp -d -t ci-XXXXXXXXXX)"
    sudo mkdir -p "$tmp_dir"
    sudo mount /dev/drbd0 "$tmp_dir"
    sudo rm -rf "$tmp_dir/"*
    sudo umount "$tmp_dir"
    sudo rm -rf "$tmp_dir"

    # drbd_stop
    exit 0
}

drbd_start()
{
    local HDD_IMG="$1"
    if [[ -z "$HDD_IMG" ]]; then
        USAGE="1";
        print_usage
    fi
    if [[ ! -f "$HDD_IMG" ]]; then 
        echo "error: file ${HDD_IMG} not found"
        exit 1
    fi

    drbd_stop
    sudo modprobe lru_cache
    sudo insmod "${DRBD_DIR}/drbd/drbd/drbd.ko"
    sudo insmod "${DRBD_DIR}/drbd/drbd/drbd_transport_tcp.ko" use_vsock=1 "vsock_cid=$VSOCK_CID"
    sudo losetup -P /dev/loop0 "${HDD_IMG}"
    sudo "${DRBDADM_BIN}" up "${DRBD_RES}"
    sudo "${DRBDADM_BIN}" secondary "${DRBD_RES}"
    "${DRBDSETUP_BIN}" show "${DRBD_RES}"
    exit 0
}

# This script assumes that we're not running as root. 
# All sorts of weird things are happening if we're running this as root, 
# and this is a quick fix to prevent them from happening.
if [[ "$EUID" -eq 0 ]]; then
  echo "Please do not run as root"
  exit 1
fi

while [[ "$#" -gt 0 ]]; do
    arg="$1"
    # Since the original script did not use double dashes, auto-upgrade any single dash
    # to a double dash
    if [[ "${arg}" =~ ^-[a-z] ]]; then
        arg="-${arg}"
    fi

    case "${arg}" in
        --build)
            BUILD="1"
            ;;
        --disk-create)
            INIT="${2}"
            shift
            ;;
        --size|--mem)
            HDD_SZ_MB="${2}"
            shift
            ;;
        --start)
            START="${2}"
            shift
            ;;
        --stop)
            STOP="1"
            ;;
        --cid)
            VSOCK_CID="${2}"
            shift
            ;;
        *)
            USAGE="1"
            ;;
    esac
    # The only way shift can fail, is if we pre-shifted for a flag that takes a value
    if ! shift; then
        echo "${arg} requires a parameter, see --help"
        exit 1
    fi
done

[[ -n "$BUILD" ]] && drbd_build
[[ -n "$INIT"  ]] && drbd_init $INIT
[[ -n "$START" ]] && drbd_start $START
[[ -n "$STOP"  ]] && drbd_stop_and_exit
[[ -n "$USAGE" ]] && print_usage

print_usage
